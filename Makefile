all: bin/test bin/exemple bin/affichage

bin/test: obj/mainTest.o obj/Pixel.o obj/Image.o
	g++ -g obj/mainTest.o obj/Pixel.o obj/Image.o -o bin/test
	
	
bin/exemple: obj/mainExemple.o obj/Pixel.o obj/Image.o
	g++ -g obj/mainExemple.o obj/Pixel.o obj/Image.o -o bin/exemple
	

bin/affichage: obj/mainAffichage.o obj/ImageViewer.o obj/Image.o obj/Pixel.o
	g++ -g obj/mainAffichage.o obj/ImageViewer.o obj/Image.o obj/Pixel.o -o bin/affichage -lSDL2 -lSDL2_ttf -lSDL2_image
	
	
obj/mainTest.o: src/mainTest.cpp src/Pixel.h src/Image.h
	g++ -g -Wall -c src/mainTest.cpp -o obj/mainTest.o
            
obj/Pixel.o: src/Pixel.cpp src/Pixel.h
	g++ -g -Wall -c src/Pixel.cpp -o obj/Pixel.o
	
obj/Image.o: src/Image.cpp src/Image.h src/Pixel.h
	g++ -g -Wall -c src/Image.cpp -o obj/Image.o
	
obj/mainExemple.o: src/mainExemple.cpp src/Image.h src/Pixel.h
	g++ -g -Wall -c src/mainExemple.cpp -o obj/mainExemple.o

	
obj/ImageViewer.o: src/ImageViewer.cpp src/ImageViewer.h src/Image.h
	g++ -g -Wall -c src/ImageViewer.cpp -o obj/ImageViewer.o -lSDL2 -lSDL2_ttf -lSDL2_image
	
obj/mainAffichage.o: src/mainAffichage.cpp src/ImageViewer.h src/Image.h
	g++ -g -Wall -c src/mainAffichage.cpp -o obj/mainAffichage.o

doc: doc/doxyfile
	cd doc ; doxygen doxyfile
 	
clean:	
	rm -rf obj/* bin/* doc/html

veryclean: clean
	rm bin/* obj/*
