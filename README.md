# L2_CDA_ModuleImage

Ce dépôt est le dépôt de départ du module Image à rendre. Son seul avantage par rapport à partir d'un dépôt vide est qu'il contient un script d'intégration continue `.gitla-ci.yml`. Ce script est exécuté par la forge à chaque `push`. Ce script télécharge et lance le script de notation de l'UE. [Voir la page du TD à ce sujet](http://alexandre.meyer.pages.univ-lyon1.fr/lifapcd/td/).

Pour commencer à travailler, il faut faire un `fork` de ce projet. Ajouter les membres du groupe.
Pour voir le résultat de l'exécution du script, il faut aller sur la forge dans le menu `Build/Jobs` et cliquer sur le dernier run.

# Module Image, UE LIFAPCD
- ID Forge : l-2-cda-module-image-jm-remy
## Contributeurs et License
- VWALUMA Jean Marc p2108190
- BECHANI Yasser p2100684
- BRENNER Remy p1809115
## Pour compiler
- Pour compiler placez vous dans le repertoire du projet (dans un terminal) et tapez la commande "make"
- Pour executer tapez soit "bin/exemple" soit "bin/test" soit "bin/affichage" depuis un terminal
## Les fichiers
- Dans le repertoire src vous trouverez les fichiers .cpp et .h
- Dans le repertoire obj vous trouverez les fichiers objets de chacun des .cpp
- Dans le repertoire bin vous trouverez 3 executables permetant de tester le module image. 
- Dans le repertoire data vous trouverez les images sauvées et lues.
- Dans le repertoire doc vous trouverez la documentation.
## Bugs
- Lors du premier test des fonctions "sauver" et "ouvrir" le repertoire "data" n'existait pas.
- Les coordonnées des pixels de l'image sauvé ne sont pas corrects, ils sont divisés par deux.
- Dans la fontion "sauver" les variables r,g,b ne sont pas des entiers (char) et ne correspondent donc pas à des couleurs pour les pixels de l'image à sauver.
- Dans la fonciton "sauver" les couleurs du fichier sources ne sont pas récupérés dans le bon ordre 
