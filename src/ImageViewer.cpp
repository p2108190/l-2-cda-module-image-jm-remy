#include <cassert>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <filesystem>
#include<fstream>

#include "ImageViewer.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include <iostream>
using namespace std;


ImageViewer::ImageViewer()
{
   m_surface = nullptr; 
   m_texture = nullptr; 

   // Initialisation de la SDL
   cout << "SDL: init" << endl;
   if (SDL_Init(SDL_INIT_VIDEO) < 0)
   {
       cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
       SDL_Quit();
       exit(1);
   }

   cout << "SDL_ttf: init" << endl;
   if (TTF_Init() != 0)
   {
       cout << "Erreur lors de l'initialisation de la SDL_ttf : " << TTF_GetError() << endl;
       SDL_Quit();
       exit(1);
   }

   int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
   cout << "SDL_image: init" << endl;
   if (!(IMG_Init(imgFlags) & imgFlags))
   {
       cout << "SDL_m_image could not initialize! SDL_m_image Error: " << IMG_GetError() << endl;
       SDL_Quit();
       exit(1);
   }

   int dimx, dimy;
   dimx = 200;
   dimy = 200;

   // Creation de la fenetre
   window = SDL_CreateWindow("Module Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, dimx, dimy, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
   if (window == NULL)
   {
       cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl;
       SDL_Quit();
       exit(1);
   }

   renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
   if (renderer == nullptr)
   {
       cout << "Erreur lors de la creation du renderer : " << SDL_GetError() << endl;
       SDL_Quit();
       exit(1);
   }

}

ImageViewer::~ImageViewer()
{
    m_surface = nullptr;
    m_texture = nullptr;

    TTF_Quit();
    SDL_FreeSurface(m_surface);
    SDL_DestroyTexture(m_texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    cout<<"imageviewer détruit"<<endl;
}


void ImageViewer::afficher(const Image & im)
{
    SDL_Event events;
    bool quit = false;
  
    im.sauver("./data/image3.ppm");

    m_surface = IMG_Load("./data/image3.ppm");
    if (m_surface == nullptr)
    {
        cout << "Erreur lors de la création de la surface : " << SDL_GetError() << endl;
       SDL_Quit();
       exit(1);
    }

    m_texture = SDL_CreateTextureFromSurface(renderer,m_surface);

    while (!quit)
    {

        // tant qu'il y a des evenements à traiter (cette boucle n'est pas bloquante)
        while (SDL_PollEvent(&events))
        {
            if (events.type == SDL_QUIT)
                quit = true; // Si l'utilisateur a clique sur la croix de fermeture
            else if (events.type == SDL_KEYDOWN)
            {
                switch (events.key.keysym.scancode)
                {
                case SDL_SCANCODE_ESCAPE:
                case SDL_SCANCODE_Q:
                    quit = true;
                    break;
                default:
                    break;
                }
            }
        }

        // on affiche le jeu sur le buffer caché
        SDL_SetRenderDrawColor(renderer, 169, 169, 169, 255);
        SDL_RenderClear(renderer);
        // on permute les deux buffers (cette fonction ne doit se faire qu'une seule fois dans la boucle)
        SDL_RenderPresent(renderer);
    }

 }
