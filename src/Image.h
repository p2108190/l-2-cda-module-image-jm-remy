#ifndef _IMAGE_H
#define _IMAGE_H
#include "Pixel.h"
#include <iostream>

using namespace std;

/**
 * @class Image
 *
 * @brief La classe Image contient deux entier dimx et dimy (dimensions de l'image), un tableau de Pixel ainsi que des fonctions et procédure permettant la manipulation de l'image et des pixels qui la composent.
 * @param dimx,dimy Entiers non-signés représentant la dimensions X et Y de l'image.
 * @param tab Tableau contenant les pixels de l'image.
 * 
 */

 /**
* \fn  Image::Image()
 * @brief Constructeur par défaut de la classe Image: initialise dimx et dimy à 0. 
 * N’alloue aucune mémoire pour le tableau de pixel.
 * 
 * @return Renvoi une Image dont les dimensions dimx et dimy sont égales à 0.
 */

/**
* \fn  Image::Image(const unsigned int &x, const unsigned int &y)
 * @brief Constructeur de la classe Image: initialise dimx et dimy avec les paramètres x et y. 
 * Alloue le tableau de pixel dans le tas (image noire).
 * 
 * @return Renvoi l'instance de l'Image crée avec les dimensions dimx et dimy étant égales à x et y et le tableau de pixel initialisé selon les dimensions.
 */

/**
* \fn  Image::~Image()
 * @brief Destructeur de la classe : déallocation de la mémoire du tableau de pixels et mise à jour des champs dimx et dimy à 0. 
 * 
 * 
 * @return Renvoi l'instance de l'Image vide avec les dimensions à 0.
 */

/**
* \fn Pixel & Image::getPix(const unsigned int &x, const unsigned int &y) const;
 * @brief Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité. 
 * 
 * @return Renvoi le Pixel de coordonnée (x,y).
 */

/**
* \fn Pixel Image::getPix2(const unsigned int &x, const unsigned int &y) const
 * @brief Récupère la copie du pixel de coordonnées (x,y) en vérifiant sa validité. 
 * 
 * @return Renvoi une copie du Pixel de coordonnée (x,y).
 */

/**
* \fn void Image::setPix(int x,int y,Pixel p)
 * @brief Modifie le pixel de coordonnées (x,y). 
 * 
 */

/**
* \fn void Image::dessinerRectangle(unsigned Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, const Pixel & couleur)
 * @brief Dessine un rectangle plein de la couleur dans l'image (en utilisant setPix, indices en paramètre compris).
 * @param p Pixel contenant la couleur qui sera remplie dans le rectangle.
 * @param Xmin, Ymin, Xmax, Ymax Dimension du rectangle à remplir.
 */

/**
* \fn void Image::effacer (const Pixel & couleur)
 * @brief Efface l'image en la remplissant de la couleur en paramètre (en appelant dessinerRectangle avec le bon rectangle). 
 * 
 */

/**
* \fn void Image::testRegression()
 * @brief Effectue une série de tests vérifiant que toutes les fonctions fonctionnent et font bien ce qu’elles sont censées faire, ainsi que les données membres de l'objet sont conformes.
 * 
 */

/**
* \fn void Image::sauver(const std::string &filename)const
 * @brief Permet de sauver les pixels de l'image dans un fichier.
 * @param filename Chemin vers le fichier dans lequel l'image sera sauver.
 */

/**
* \fn void Image::ouvrir(const std::string &filename)
 * @brief Permet de récupérer les pixels de l'image dans un fichier.
 * @param filename Chemin vers le fichier depuis lequel l'image sera récuperer.
 */

/**
* \fn void Image::afficherConsole()
 * @brief Permet d'afficher les valeurs des pixels de l'image sur la console
 *
 */

class Image
{
    private:
        unsigned int dimx;
        unsigned int dimy;
        Pixel* tab;
    

    public:
        Image();
        Image(unsigned int dimensionX, unsigned int dimensionY);
        ~Image();
        static void testRegression();
        Pixel & getPix(unsigned int x, unsigned int y);
        Pixel getPix2(unsigned int x, unsigned int y)const;
        void setPix(const unsigned int &x, const unsigned int &y, const Pixel &couleur);
        void dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, const Pixel & couleur);
        void effacer (const Pixel & couleur);
        void sauver(const string &filename) const;
        void ouvrir(const string &filename);
        void afficherConsole();

    
    
};





#endif


