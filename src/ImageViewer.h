#ifndef _IMAGEVIEWER_H
#define _IMAGEVIEWER_H

#include "Image.h"
#include <SDL2/SDL.h>

/**
 * @class ImageViewer
 * @brief La classe ImageViewer permettant d'afficher une image avec SDL2.
 *
 */

/**
* \fn  ImageViewer::ImageViewer()
 * @brief Constructeur qui initialise tout SDL2 et crée la fenêtre et le rendere.
 */

 /**
  * \fn ImageViewer::~ImageViewer()
  * @brief Destructeur de la classe ImageViewer.
  * Libère la mémoire allouée et ferme les bibliothèques SDL utilisées.
  */

/**
  * \fn ImageViewer::afficher(const Image & im)
  * @brief Affiche l'image dans la fenêtre SDL.
  * @param im Image à afficher dans la fenêtre.
  */




class ImageViewer
{
    private :
    SDL_Window * window; ///< Lien vers la fenêtre SDL
    SDL_Renderer * renderer; ///< Lien vers le renderer
    SDL_Surface * m_surface; ///< Lien vers la surface
    SDL_Texture * m_texture; ///< Lien vers la texture
    

    public :
    ImageViewer(); ///< Constructeur qui initialise tout SDL2 et crée la fenêtre et le renderer
    ~ImageViewer(); ///< Détruit et ferme SDL2
    void afficher(const Image & im);

};




#endif
