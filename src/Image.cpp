#include "Image.h"
#include <iostream>
#include <cassert>
#include <fstream>

using namespace std;

Image::Image()
{
    cout<<"tab alloué : "<<tab<<endl;
    tab = nullptr;
    dimx = 0;
    dimy = 0;

}

Image::Image(unsigned int dimensionX, unsigned int dimensionY)
{
    assert (dimensionX>0 && dimensionY>0);

    dimx = dimensionX;
    dimy = dimensionY;
    tab = new Pixel [dimx*dimy];
    for(unsigned int i = 0; i<dimx*dimy; i++)
    {
        tab[i] = Pixel(0,0,0);
    }

}

Image::~Image()
{
    dimx = 0;
    dimy = 0;
    if (tab != nullptr)
    {
        delete [] tab;
        tab = nullptr;
    }
    
    
    cout<<"tab détruit : "<<tab<<endl;
}


Pixel & Image::getPix(unsigned int x, unsigned int y)
{
    assert (x>=0 && y>=0);
    assert (x<dimx && y<dimy);

    return tab[y*dimx+x];
}

Pixel Image::getPix2(unsigned int x, unsigned int y) const
{
    assert (x>=0 && y>=0); 
    assert (x<dimx && y<dimy);

    Pixel ret = tab[y*dimx+x];

    return ret;
}

void Image::setPix(const unsigned int &x, const unsigned int &y, const Pixel &couleur)
{
    tab[y*dimx+x] = couleur;
}

void Image::dessinerRectangle(unsigned int Xmin, unsigned int Ymin, unsigned int Xmax, unsigned int Ymax, const Pixel & couleur)
{
    unsigned int i,j;

    for(i=Xmin;i<Xmax;i++)
    {
        for(j=Ymin;j<Ymax;j++)
        {
            setPix(i,j,couleur);

        }
    }
}

void Image::effacer (const Pixel & couleur)
{
    dessinerRectangle(0,0,dimx,dimy,couleur);
}

void Image::testRegression()
{
    cout<<"test de regression : "<<endl;
    Image im1;
    assert (im1.dimx == 0);
    assert (im1.dimy == 0);

    Image im2(50,50);
    assert (im2.dimx == 50);
    assert (im2.dimy == 50);

    Pixel p1 = im2.getPix(14,26);
    assert(p1.r == 0);
    assert(p1.g == 0);
    assert(p1.b == 0);

    Pixel p2(145,78,153);
    im2.dessinerRectangle(10,10,30,30,p2);
    p1 = im2.getPix(18,22);
    cout<<int(p1.r)<<" "<<int(p1.g)<<" "<<int(p1.b)<<endl;

    im2.effacer(Pixel(0,0,0));
    p2 = im2.getPix(18,22);
    cout<<int(p2.r)<<" "<<int(p2.g)<<" "<<int(p2.b)<<endl;



    

    cout<<"fin test"<<endl;
}

void Image::sauver(const string &filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (unsigned int y = 0; y < dimy; y++)
        for (unsigned int x = 0; x < dimx; x++)
        {
            Pixel pix = getPix2(x, y);
            fichier << int(pix.r) << " " << int(pix.g) << " " << int(pix.b) << " ";
            //cout<<x<<" , "<<y<<endl;
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string &filename)
{
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    unsigned char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    cout<<dimx<<","<<dimy<<endl;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; y++)
        for (unsigned int x = 0; x < dimx; x++)
        {
            fichier >> r >> g >> b;
            //cout<<r<<" , "<<g<<" , "<<b<<endl;
            getPix(x, y).r = char(r);
            getPix(x, y).g = char(g);
            getPix(x, y).b = char(b);
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()
{
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel &pix = getPix(x, y);
            cout << pix.r << " " << pix.g << " " << pix.b << " ";
        }
        cout << endl;
    }
}

