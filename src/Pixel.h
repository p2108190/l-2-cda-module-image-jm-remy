#ifndef _PIXEL_H
#define _PIXEL_H

/**
 * @class Pixel
 *
 * @brief La classe Pixel contient trois données membres qui sont des entiers non-signés. Ces trois entiers représentent la couleur du pixel sous la forme RGB.
 *
 */

/**
 * \fn  Pixel::Pixel(int x,int y, int z)
 * @brief Constructeur de la classe pixel : initialise r,g,b avec les paramètres.
 * 
 * @param x Le niveau de rouge.
 * @param y Le niveau de vert.
  * @param y Le niveau de bleu.
 * @return Renvoi un pixel dont les données membres r,g,b sont initialisé avec les paramètres.
 */

/**
* \fn  Pixel::Pixel()
 * @brief Constructeur par défaut de la classe pixel: initialise le pixel à la couleur noire.
 * 
 * @return Renvoi un pixel dont les données membres r,g,b sont égales à 0.
 */


struct Pixel
{
    unsigned char r; ///< caractère non-signés représentant le niveau de rouge dans la couleur du Pixel.
    unsigned char g; ///< caractère non-signés représentant le niveau de vert dans la couleur du Pixel.
    unsigned char b; ///< caractère non-signés représentant le niveau de bleu dans la couleur du Pixel.

    Pixel();
    Pixel(unsigned int nr, unsigned int ng, unsigned int nb);   
};

#endif