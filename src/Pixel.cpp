#include "Pixel.h"
#include <cassert>
#include <iostream>

Pixel::Pixel()
{
    r = 0;
    g = 0;
    b = 0;
}

Pixel::Pixel(unsigned int nr, unsigned int ng, unsigned int nb) 
{
    assert(nr>=0 && nr<=255);
    assert(ng>=0 && ng<=255);
    assert(nb>=0 && nb<=255);
    r = char(nr);
    g = char(ng);
    b = char(nb);
}


